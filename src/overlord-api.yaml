openapi: 3.0.0
info:
  title: Overlord API
  description: Overlord API documentation
  version: 1.0.0
servers:
  - url: http://api.operation-overlord.online
    description: Staging server URL
  - url: http://api-mock.operation-overlord.online
    description: Mock server URL
  - url: http://localhost:3000
    description: Developer URL
paths:
  /workspaces:
    get:
      tags:
        - Workspace
      summary: Returns a list of workspaces.
      description: A full list of all Workspaces.
      responses:
        200:
          description: A JSON array of Workspaces
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Workspace'
              examples:
                value:
                  $ref: '#/components/examples/ExampleWorkspaceArray'

    post:
      tags:
        - Workspace
      summary: Create a workspace.
      description: Create a workspace and return a JSON with the workspace.
      requestBody:
        description: Workspace object that needs to be added
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Workspace'
            examples:
              value:
                $ref: '#/components/examples/ExampleBaseWorkspace'
        required: true
      responses:
        201:
          description: A JSON of the Workspace that was created.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Workspace'
              examples:
                value:
                  $ref: '#/components/examples/ExampleWorkspace'
        400:
          $ref: '#/components/responses/BadRequest'

  /workspaces/{workspaceId}:
    get:
      tags:
        - Workspace
      summary: Returns all properties from a workspace.
      description: Returns all properties from a workspace based on the workspace Id.
      parameters:
        - $ref: '#/components/parameters/workspaceId'
      responses:
        200:
          description: A JSON Workspace object
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Workspace'
              examples:
                value:
                  $ref: '#/components/examples/ExampleWorkspace'
        400:
          $ref: '#/components/responses/BadRequest'
        404:
          $ref: '#/components/responses/NotFound'

    put:
      tags:
        - Workspace
      summary: Update a workspace.
      description: Update a workspace by id and returns the workspace updated
      parameters:
        - $ref: '#/components/parameters/workspaceId'
      requestBody:
        description: Workspace object that needs to be updated
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Workspace'
            examples:
              value:
                $ref: '#/components/examples/ExampleBaseWorkspace'
        required: true
      responses:
        200:
          description: Workspace updated successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Workspace'
              examples:
                value:
                  $ref: '#/components/examples/ExampleWorkspace'
        400:
          $ref: '#/components/responses/BadRequest'
        404:
          $ref: '#/components/responses/NotFound'

  /channels:
    get:
      tags:
        - Channel
      summary: Returns a list of channels.
      description: A full list of all Channels.
      responses:
        200:
          description: A JSON array of Channels
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Channel'
              examples:
                value:
                  $ref: '#/components/examples/ExampleChannelArray'
        400:
          $ref: '#/components/responses/BadRequest'
        404:
          $ref: '#/components/responses/NotFound'

    post:
      tags:
        - Channel
      summary: Creates a new channel
      description: Endpoint used to create a new channel inside a workspace.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Channel'
            examples:
              value:
                $ref: '#/components/examples/ExampleBaseChannel'

      responses:
        201:
          description: Channel created succesfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Channel'
              examples:
                value:
                  $ref: '#/components/examples/ExampleChannel'
        400:
          $ref: '#/components/responses/BadRequest'

  /channels/{channelId}:
    get:
      tags:
        - Channel
      summary: Returns all properties from a channel.
      description: This API returns all properties from a channel based on the channel Id and the workspace Id.
      parameters:
        - $ref: '#/components/parameters/channelId'
      responses:
        200:
          description: A JSON Channel object
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Channel'
              examples:
                value:
                  $ref: '#/components/examples/ExampleChannel'
        400:
          $ref: '#/components/responses/BadRequest'
        404:
          $ref: '#/components/responses/NotFound'

    put:
      tags:
        - Channel
      summary: Update a channel
      description: Update the whole channel according its channel id
      parameters:
        - $ref: '#/components/parameters/channelId'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Channel'
            examples:
              value:
                $ref: '#/components/examples/ExampleBaseChannel'

      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Channel'
              examples:
                value:
                  $ref: '#/components/examples/ExampleChannel'
        400:
          $ref: '#/components/responses/BadRequest'

    delete:
      tags:
        - Channel
      summary: Deletes a Channel
      description: This API delete a channel based on the channel Id and the workspace Id.
      parameters:
        - $ref: '#/components/parameters/channelId'
      responses:
        204:
          description: The deleted channel
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Channel'
              examples:
                value:
                  $ref: '#/components/examples/ExampleChannelDeleted'
        400:
          $ref: '#/components/responses/BadRequest'
        404:
          $ref: '#/components/responses/NotFound'

components:
  schemas:
    Workspace:
      type: object
      properties:
        id:
          type: string
        name:
          type: string
        description:
          type: string

    Channel:
      type: object
      properties:
        id:
          type: string
        workspaceId:
          type: string
        name:
          type: string
        description:
          type: string
        type:
          type: string

    Error:
      type: object
      properties:
        message:
          type: string

  parameters:
    workspaceId:
      in: path
      name: workspaceId
      required: true
      schema:
        type: string
        minimum: 1
    channelId:
      in: path
      name: channelId
      required: true
      schema:
        type: string
        minimum: 1

  responses:
    BadRequest:
      description: Invalid attribute
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
          examples:
            Error:
              $ref: '#/components/examples/ExampleErrorResponseBadRequest'

    NotFound:
      description: The specified resource was not found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
          examples:
            Error:
              $ref: '#/components/examples/ExampleErrorResponseNotFound'

  examples:
    ExampleBaseWorkspace:
      value:
        name: My_Workspace
        description: Your default workspace

    ExampleWorkspace:
      value:
        id: 0142385c-ca2a-4c4b-ad6e-8f5d77504e1c
        name: My_Workspace
        description: Your default workspace

    ExampleWorkspaceArray:
      value:
        - id: 0142385c-ca2a-4c4b-ad6e-8f5d77504e1c
          name: My_Workspace
          description: Your default workspace
        - id: aee05d28-d0d3-4448-a1c1-53b5af061187
          name: NB02
          description: This is the workspace for National Bootcamp

    ExampleChannelArray:
      value:
        - id: 9a1396b2-d3de-4730-8ec4-d44dc88b2f9e
          name: General
          description: General channel for work chat
          workspaceId: d081f5b8-ad3c-4da0-95c4-5a4a3ede7206
          type: text
        - id: 35c32c8e-5f7d-464c-a4c3-06db05ce0398
          name: Standups
          description: This channel is only for Standups!
          workspaceId: d081f5b8-ad3c-4da0-95c4-5a4a3ede7206
          type: audio
        - id: cba67083-fcf3-40ae-90fe-ee9d498ac478
          name: Gitlab
          description: All Gitlab activity notifications
          workspaceId: d081f5b8-ad3c-4da0-95c4-5a4a3ede7206
          type: text
        - id: cba67083-fcf3-40ae-90fe-ee9d498ac478
          name: Random
          description: Say what is on your mind
          workspaceId: d081f5b8-ad3c-4da0-95c4-5a4a3ede7206
          type: audio

    ExampleChannel:
      value:
        id: cba67083-fcf3-40ae-90fe-ee9d498ac478
        name: Random
        description: Say what is on your mind
        workspaceId: d081f5b8-ad3c-4da0-95c4-5a4a3ede7206
        type: audio
    ExampleBaseChannel:
      value:
        name: Random
        description: Say what is on your mind
        type: audio
    ExampleChannelDeleted:
      value:
        id: cba67083-fcf3-40ae-90fe-ee9d498ac478
        name: Random
        description: Say what is on your mind
        workspaceId: d081f5b8-ad3c-4da0-95c4-5a4a3ede7206
        type: audio

    ExampleErrorResponseBadRequest:
      value:
        message: Bad Request

    ExampleErrorResponseNotFound:
      value:
        message: Resource was not found
