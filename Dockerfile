FROM swaggerapi/swagger-ui
ADD ./build/swagger.json /oas/
ENV SWAGGER_JSON "/oas/swagger.json"
ENV PORT 80
